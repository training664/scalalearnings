object QueueImplementation {

  def main(args : Array[String]) = {
    var queue = new MyQueue[String]
    queue.add("R")
    queue.add("S")
    queue.add("T")
    queue.addFirst("P")
    queue.addLast("E")
    queue.push("S")
    println("Display Queue: ")
    queue.display
    println("...")
    println("Queue contains R: " + queue.contains("R"))
    println("Queue contains X: " + queue.contains("X"))
    println("...")
    println("Element(head):" + queue.element)
    println("getFirst: " + queue.getFirst)
    println("getLast: " + queue.getLast)
    println("Peek: " + queue.peek)
    println("peekFirst: " + queue.peekFirst)
    println("peekLast: " + queue.peekLast)
    println("...")
    println("Display Queue: ")
    queue.display
    println("...")
    println("poll: " + queue.poll)
    println("Display Queue: ")
    queue.display
    println("...")
    println("pollFirst: " + queue.pollFirst)
    println("Display Queue: ")
    queue.display
    println("...")
    println("pollLast: " + queue.pollLast)
    println("Display Queue: ")
    queue.display
    println("...")
    println("pop: " + queue.pop)
    println("Display Queue: ")
    queue.display
    println("...")
    println("remove: " + queue.remove)
    println("Display Queue: ")
    queue.display
    println("...")

  }
}
