object MyLinkedHashMap{
  class Node[K,V](var key: K, var value: V, var next: Node[K, V]) {
    var before: MyLinkedHashMap.Node[K, V] = null
    var after: MyLinkedHashMap.Node[K, V] = null
  }
}


class MyLinkedHashMap[K, V]() {

  var initialCapacity = 16
  var map = new Array[MyLinkedHashMap.Node[K, V]](initialCapacity)
  var head :  MyLinkedHashMap.Node[K, V] = null
  var tail: MyLinkedHashMap.Node[K, V] = null

  private def hash(key:K): Int = {
    Math.abs(key.hashCode()) % initialCapacity
  }

  def put(key: K, value: V): Unit = {
    if (key == null) {
      return;
    }
    var hashIndex = hash(key)
    var newNode = new MyLinkedHashMap.Node[K, V](key, value, null)
    addtoInsertionOrder(newNode)
    if(map(hashIndex)==null){
      map(hashIndex) = newNode
    }
    else{
      var prev : MyLinkedHashMap.Node[K, V] = null
      var curr : MyLinkedHashMap.Node[K, V] = map(hashIndex)
      while(curr != null){
        if(curr.key.equals(key)){
          if(prev == null){
            newNode.next = curr.next
            map(hashIndex) = newNode
            return
          }
          else{
            newNode.next = curr.next
            prev.next = newNode
          }
        }
        prev = curr
        curr = curr.next
      }
      prev.next = newNode
    }
  }

  def get(key: K): V = {
    var empty : MyLinkedHashMap.Node[K, V] = null
    if(map(hash(key)) == null) empty.value
    else {
      var curr : MyLinkedHashMap.Node[K, V] = map(hash(key))
      while(curr != null) {
        if (curr.key.equals(key)) {
          return curr.value
        }
        curr = curr.next
      }
      empty.value
    }
  }

  def remove(key: K): Boolean = {
    var hashIndex: Int = hash(key)
    if(map(hashIndex)==null) false
    else {
      var prev: MyLinkedHashMap.Node[K, V] = null
      var curr: MyLinkedHashMap.Node[K, V] = map(hashIndex)
      while (curr != null) {
        if (curr.key.equals(key)) {
          deleteFromInsertionOrder(curr)
          if (prev == null) {
            map(hashIndex) = map(hashIndex).next
            return true
          }
          else {
            prev.next = curr.next
            return true
          }
        }
        prev = curr
        curr = curr.next
      }
      false
    }
  }

  def addtoInsertionOrder(newNode: MyLinkedHashMap.Node[K, V]): Unit ={
    if(head == null){
      head = newNode
      tail = newNode
      return
    }
    if(head.key.equals(newNode.key)){
      deleteFirst();
      insertFirst(newNode)
      return
    }
    if(tail.key.equals(newNode.key)){
      deleteLast();
      insertLast(newNode)
      return
    }
    var before: MyLinkedHashMap.Node[K,V] = deleteElement(newNode)
    if(before == null) insertLast(newNode)
    else insertAfter(before,newNode)
  }

  private def deleteFromInsertionOrder(newNode: MyLinkedHashMap.Node[K, V]): Unit = {
    if (head.key.equals(newNode.key)) {
      deleteFirst()
      return
    }
    if (tail.key.equals(newNode.key)) {
      deleteLast()
      return
    }
    deleteElement(newNode)
  }

  private def insertAfter(before: MyLinkedHashMap.Node[K, V], newNode: MyLinkedHashMap.Node[K, V]): Unit = {
    var curr : MyLinkedHashMap.Node[K,V] = head
    while ( curr != before){
      curr = curr.after
    }
    newNode.after = before.after
    before.after.before = newNode
    newNode.before = before
    before.after = newNode
  }

  private def deleteElement(newNode: MyLinkedHashMap.Node[K, V]): MyLinkedHashMap.Node[K, V] = {
    var curr = head
    while (!curr.key.equals(newNode.key)) {
      if (curr.after == null) return null
      curr = curr.after
    }
    val before = curr.before
    curr.before.after = curr.after
    curr.after.before = curr.before
    before
  }

  private def insertLast(newNode: MyLinkedHashMap.Node[K, V]): Unit = {
    if (head == null) {
      head = newNode
      tail = newNode
      return
    }
    tail.after = newNode
    newNode.before = tail
    tail = newNode
  }

  private def deleteLast(): Unit = {
    if (head eq tail) {
      head = null
      tail = null
      return
    }
    tail = tail.before
    tail.after = null
  }

  private def insertFirst(newNode: MyLinkedHashMap.Node[K, V]): Unit = {
    if (head == null) {
      head = newNode
      tail = newNode
      return
    }
    newNode.after = head
    head.before = newNode
    head = newNode
  }

  private def deleteFirst(): Unit = {
    if (head == tail) {
      head = null
      tail = null
      return
    }
    head = head.after
    head.before = null
  }


  def display(): Unit = {
    var curr = head
    if (curr == null) println("[]")
    while ( curr != null) {
      println(curr.key + " " + curr.value)
      curr = curr.after
    }
  }

  def entrySet: Set[String] = {
    var entries : Set[String] = Set()
    var curr = head
    while ( curr != null) {
      entries += "{ " + curr.key + ":" + curr.value + " }"
      curr = curr.after
    }
    entries
  }

  def keySet: Set[K] = {
    var keys : Set[K] = Set()
    var curr = head
    while ( curr != null) {
      keys += curr.key
      curr = curr.after
    }
    keys
  }

  def values: Set[V] = {
    var valuesSet :  Set[V] = Set()
    var curr = head
    while ( curr != null) {
      valuesSet += curr.value
      curr = curr.after
    }
    valuesSet
  }

  def containsKey(key: K): Boolean = {
    var curr = head
    while ( curr != null){
      if (curr.key.equals(key)) return true
      curr = curr.after
    }
    false
  }

  def containsValue(value: V): Boolean = {
    var curr = head
    while ( curr != null){
      if (curr.value.equals(value)) return true
      curr = curr.after
    }
    false
  }

  def size: Int = {
    var length = 0
    var curr = head
    while ( curr != null) {
      length += 1
      curr = curr.after
    }
    length
  }

  def isEmpty: Boolean = {
    val length = 0
    val curr = head
    if (curr == null) return true
    false
  }

  def clear(): Unit = {
    head = null
  }

  def putIfAbsent(key: K, value: V): Unit = {
    if (key == null) return
    var hashIndex = hash(key)
    var newNode = new MyLinkedHashMap.Node[K, V](key, value, null)
    if (map(hashIndex) == null) {
      map(hashIndex) = newNode
      addtoInsertionOrder(newNode)
    }
    else {
      var prev : MyLinkedHashMap.Node[K,V] = null
      var curr = map(hashIndex)
      while (curr != null) {
        if (curr.key.equals(key)) return
        prev = curr
        curr = curr.next
      }
      addtoInsertionOrder(newNode)
      prev.next = newNode
    }
  }

  def putAll(newMap: MyLinkedHashMap[K, V]): Unit = {
    var newMapNode = newMap.head
    while ( newMapNode != null) {
      val key = newMapNode.key
      val value = newMapNode.value
      if (key == null) return
      var hashIndex = hash(key)
      var newNode = new MyLinkedHashMap.Node(key, value, null)
      addtoInsertionOrder(newNode)
      if (map(hashIndex) == null) map(hashIndex) = newNode
      else {
        var prev : MyLinkedHashMap.Node[K, V] = null
        var curr = map(hashIndex)
        while ( curr != null) {
          if (curr.key.equals(key)) if (prev == null) {
            newNode.next = curr.next
            map(hashIndex) = newNode
          }
          else {
            newNode.next = curr.next
            prev.next = newNode
          }
          prev = curr
          curr = curr.next
        }
        prev.next = newNode
      }
      newMapNode = newMapNode.after
    }
  }



}





