import scala.io.StdIn.{readInt, readLine}

object LongestIncSubsequence {
    def main(args : Array[String]): Unit ={
      var size = readInt()
      var inputArray = readLine().split(" ").map(_.toInt)
      var longestLength = 0
      var startIndex = 0
      var endIndex = 0
      for( i <- 0 until size){
        var j = i
        var arrayLen = 0
        while (j + 1 < size && inputArray(j + 1) > inputArray(j)) {
           arrayLen += 1
           j += 1
        }
        if( arrayLen > longestLength){
          longestLength = arrayLen
          startIndex = i
          endIndex = j+1
        }
      }
      for(i <- startIndex until endIndex)
        print(inputArray(i)+" ")
    }
}