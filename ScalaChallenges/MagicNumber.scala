import scala.io.StdIn.{readInt, readLine}


object MagicNumber{

  def isPrime(num: Int): Boolean ={

    for(i <- 2 to num/2){
        if(num%i==0) return false
    }
    return true
  }
  def magicNumber(num: Int): Boolean ={
    for(i <- 4 to num){
      println(i)
      if(num%i==0 && isPrime(i) && i!=5){
         return false
      }
    }
    return true
  }
  def main(args: Array[String]) {
    println("Enter array Elements: ")
    var list = readLine.split(" ").map(_.toInt)
    println("Enter subarray Size: ")
    var size = readInt()
    var minAvg = math.ceil(list.slice(0, size).sum.toFloat /size)
    for (i <- 0 until list.length) {
      var avg = math.ceil(list.slice(i, i+size).sum.toFloat /size)
      if( i+size <= list.length && minAvg > avg){
        minAvg = avg
      }
    }
    if(magicNumber(minAvg.toInt)){
      println("True")
    }
    else{
      println("False")
    }
  }
}
//  3 8 16 19 1 20 18 28 26 18
// 11 24 2 6 27 9 26 20 8 4