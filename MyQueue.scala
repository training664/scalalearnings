class MyQueue[E] {
  private var key: Int = 0
  private var queue = new MyLinkedHashMap[Int, E]
  private var tempQueue = new MyLinkedHashMap[Int, E]
  private var front : MyLinkedHashMap[Int, E] = null
  private var rear : MyLinkedHashMap[Int, E] = null

  def add(element: E): Unit ={
    queue.put(key, element)
    key+=1
  }

  def addFirst(element: E): Unit ={
    tempQueue.put(key, element)
    tempQueue.putAll(queue)
    queue = tempQueue
    key+=1
  }

  def addLast(element: E): Unit ={
    this.add(element)
  }

  def contains (element : E) :Boolean = {
    queue.containsValue(element)
  }

  def display (): Unit = {
    queue.display()
  }

  def getFirst(): E = {
    var head = queue.head
    head.value
  }

  def getLast(): E = {
    var tail = queue.tail
    tail.value
  }

  def element(): E ={
    getFirst()
  }

  def peek(): E = {
    if(queue.isEmpty) null
    getFirst()
  }

  def peekFirst() : E = {
    if(queue.isEmpty)null
    return getFirst()
  }

  def peekLast() : E = {
    if(queue.isEmpty)null
    getLast()
  }

  def poll() : E = {
    if(queue.isEmpty) null
    var element : E = getFirst()
    var head = queue.head
    queue.remove(head.key)
    element
  }

  def pollFirst : E = {
    poll()
  }

  def pollLast : E = {
    if(queue.isEmpty)null
    var element : E = getLast()
    var tail = queue.tail
    queue.remove(tail.key)
    element
  }

  def pop : E = {
    if(queue.isEmpty) throw new NoSuchElementException
    var element = getLast()
    var tail = queue.tail
    queue.remove(tail.key)
    element
  }

  def push(element: E) = {
    try {
      queue.put(key, element)
      key += 1
    }catch{
      case e: IllegalStateException => throw e
    }
  }

  def remove = {
    poll()
  }

  def size = {
    queue.size
  }

}