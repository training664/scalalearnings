object LHMImplementation {

  def main(args: Array[String]) {

    val map1 = new MyLinkedHashMap[String, String]
    println(map1.isEmpty)
    map1.put("2","R")
    map1.put("3", "F")
    println("Map1 Contents : ")
    map1.display()
    println("Map1 is empty : " + map1.isEmpty)
    println("GET method: " + map1.get("3"))
    map1.put("3", "T")
    println("Map1 Contents : ")
    map1.display
    println("Map1 PutifAbsent Example1 : ")
    map1.putIfAbsent("3", "GH")
    map1.display
    println("Map1 PutifAbsent Example1 : ")
    map1.putIfAbsent("13", "GE")
    map1.display
    map1.put("3", "K")
    map1.put("7", "K")
    map1.put("8", "K")
    map1.put("3", "P")
    println("Map1 KeySet : " + map1.keySet)
    println("Map1 EntrySet : " + map1.entrySet)
    println("Map1 Contents Before Remove : ")
    map1.display
    map1.remove("3")
    println("Map1 Contents After Remove : ")
    map1.display
    val map2 = new MyLinkedHashMap[String, String]
    map2.put("34", "G")
    map2.put("7", "F")
    println("Map2 Contents : ")
    map2.display()
    map2.putAll(map1)
    println("Map2 Contents After putAll : ")
    map2.display()
    map2.clear()
    println("Map2 Contents After Clear : ")
  }

}
